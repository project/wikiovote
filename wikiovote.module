<?php
/**
 * Wikio vote : a Drupal module that adds a "Wikio vote" button to your nodes.
 * (The code of this module is partially based on Digg This module by Ramiro G�mez)
 * @author Arnaud 'Narno' Ligny <arnaud.ligny@narno.com>
 * @copyright Narno.com
 */

/**
 * Implementation of hook_perm().
 */
function wikiovote_perm() {
  return array('use wikiovote');
}

/**
 * Implementation of hook_menu().
 */
function wikiovote_menu($may_cache) {
  $items = array();
  $items[] = array(
    'path' => 'admin/settings/wikiovote',
    'title' => t('Wikio vote'),
    'description' => t('Enable the node types and set the properties for the Wikio vote button.'),
    'callback' => 'drupal_get_form',
    'callback arguments' => array('wikiovote_admin_settings'),
    'access' => user_access('administer site configuration'),
    'type' => MENU_NORMAL_ITEM
  );
  return $items;
}

/**
 * admin settings for the wikiovote module
 */
function wikiovote_admin_settings() {
  $module_path = drupal_get_path('module', 'wikiovote');
  $form = array();
  $form['wikiovote'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings')
  );
  $form['wikiovote']['wikiovote_button_type'] = array(
    '#type' => 'radios',
    '#title' => t('Button types'),
    '#default_value' => variable_get('wikiovote_button_type', 'big'),
    '#options' => array('big' => '<img src="'. url($module_path .'/images/wikiovote_big.png') .'" alt="big" />', 'inline' => '<img src="'. url($module_path .'/images/wikiovote_inline.png') .'" alt="inline" />'),
    '#description' => t('Specifies the button type (also named <em>clicker type</em>)')
  );
  $form['wikiovote']['wikiovote_button_weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight'),
    '#default_value' => variable_get('wikiovote_button_weight', 0),
    '#options' => drupal_map_assoc(range(-20,20)),
    '#description' => t('Specifies the position of the <em>Wikio vote</em> button (a low weight will display the button above the content and a high weight, below the content)')
  );
  $form['wikiovote']['wikiovote_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types'),
    '#default_value' => variable_get('wikiovote_node_types', array()),
    '#options' => node_get_types('names'),
    '#description' => t('Activate the node types in where the <em>Wikio vote</em> button shall be displayed')
  );
  $form['wikiovote']['wikiovote_where'] = array(
    '#type' => 'select',
    '#title' => t('Where to display'),
    '#default_value' => variable_get('wikiovote_where', 0),
    '#options' => array(t('Nodes'), t('Links'))
  );
  $form['wikiovote']['wikiovote_in_node'] = array(
    '#type' => 'select',
    '#title' => t('When to display in nodes'),
    '#default_value' => variable_get('wikiovote_in_node', 0),
    '#options' => array(0 => t('Disabled'), 1 => t('Teaser view'), 2 => t('Full page view'), 3 => t('Teaser and full page view')),
    '#description' => t('When to display the <em>Wikio vote</em> button in the node')
  );
  if (variable_get('wikiovote_where', 0) == 1) {
    $form['wikiovote']['wikiovote_in_node']['#disabled'] = TRUE;
  }
  return system_settings_form($form);
}

/**
 * Implementation of hook_nodeapi().
 */
function wikiovote_nodeapi(&$node, $op, $teaser, $page) {
  if ($op == 'view' && user_access('use wikiovote') && variable_get('wikiovote_where', 0) == 0) {
    // node types ?
    if(in_array($node->type, variable_get('wikiovote_node_types', array()), TRUE)) {
      // when in node ?
      switch (variable_get('wikiovote_in_node', 0)) {
        case 1:
          if ($teaser) {
            $node->content['wikiovote_button'] = array(
              '#value'  => theme('wikiovote_button', $node),
              '#weight' => variable_get('wikiovote_button_weight', 0)
            );
          }
        break;
        case 2:
          if ($page) {
            $node->content['wikiovote_button'] = array(
              '#value'  => theme('wikiovote_button', $node),
              '#weight' => variable_get('wikiovote_button_weight', 0)
            );
          }
        break;
        case 3:
          if ($teaser) {
            $node->content['wikiovote_button'] = array(
              '#value'  => theme('wikiovote_button', $node),
              '#weight' => variable_get('wikiovote_button_weight', 0)
            );
          } elseif ($page) {
            $node->content['wikiovote_button'] = array(
              '#value'  => theme('wikiovote_button', $node),
              '#weight' => variable_get('wikiovote_button_weight', 0)
            );
          }
        break;
      }
    }
  }
}

/**
 * Implementation of hook_link().
 */
function wikiovote_link($type, $node = NULL, $teaser = FALSE) {
  $links = array();
  if (user_access('use wikiovote') && variable_get('wikiovote_where', 0) == 1) {
    // node types ?
    if(in_array($node->type, variable_get('wikiovote_node_types', array()), TRUE)) {
      $links['wikiovote_link'] = array(
        'title' => theme('wikiovote_button', $node, FALSE),
        'html'  => TRUE
      );
    }
  }
  return $links;
}

/**
 * theme function for button display
 */
function theme_wikiovote_button($node) {
  $module_path = drupal_get_path('module', 'wikiovote');
  // button type
  $button_type = variable_get('wikiovote_button_type', 'big');
  // add css file
  if ($button_type == 'big') {
    drupal_add_css($module_path . '/wikiovote_big.css');
  } elseif ($button_type == 'inline') {
    drupal_add_css($module_path . '/wikiovote_inline.css');
  }
  // absolute url of the current node
  $url = url('node/'. $node->nid, NULL, NULL, TRUE);
  // vote button
  return _wikiovote($url, $button_type);
}

function _wikiovote($url, $button_type) {
  $wikioThisUrl   = $url;
  $result         = '';
  $wikioSuffix    = 'fr';
  $wikioUrl       = 'web.wikio.'. $wikioSuffix;
  $wikioNote      = 0;
  $wikioId        = 0;
  $wikioPattern1  = '';
  $wikioPattern2  = '';
  $wikioPattern3  = '/article=';
  $wikiohasVoted  = '';
  $bVoted         = 0;

  // referer
  $wikioReferer = htmlspecialchars(strip_tags('http://'. $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']), ENT_QUOTES);

  // poster IP
  if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $wikioCliIp = $_SERVER['HTTP_X_FORWARDED_FOR'];
  } else if (isset($_SERVER['HTTP_CLIENT_IP'])) {
    $wikioCliIp = $_SERVER['HTTP_CLIENT_IP'];
  } else{
    $wikioCliIp = $_SERVER['REMOTE_ADDR'];
  }

  // vote status
  $wikioRequest = 'http://'. $wikioUrl .'/getnote?u='. md5($wikioThisUrl) .'&i='. $wikioCliIp .'&referer='. md5($wikioReferer);
  $wn = drupal_http_request($wikioRequest);

  if ($wn->code == 200 && $wikioNote = $wn->data) {
    if (ereg("([0-9]*)\|([0-9]*)\|(.*)\|(.*)\|([0-9]*)\|(.*)\|([0-9]*)\|", $wikioNote, $res)) {
      $wikioNote      = $res[1];
      $wikioId        = $res[2];
      $wikioPattern1  = ($res[5]==1) ? $res[4] : $res[3];
      $wikioPattern2  = $res[4];
      $wikiohasVoted  = ($res[5]==0 && $res[7]==1) ? '&vote=1' : '';
      $bVoted         = $res[5];
      $wikioSuffix    = $res[6];
      $wikioAllowVote = $res[7];
    }
  }
  $wikioUrl = 'www.wikio.'. $wikioSuffix;
  if ($wikioId > 0) {
    $result .= "<script type=\"text/javascript\">
    var wikiovoted = false;
    function setwikio() {";
    if ($wikioAllowVote > 0 && $bVoted == 0) {
      if ($button_type == 'big') {
        $result .= "t=top.document;
        if (!wikiovoted) {
          n = t.getElementById('wikionote');
          a = t.getElementById('wikioaction');
        if (n && !isNaN(parseInt(n.innerHTML))) n.innerHTML = parseInt(n.innerHTML)+1;
        if (a) a.innerHTML = '". $wikioPattern2 ."';
        }
        wikiovoted = true;";
      } elseif ($button_type == 'inline') {
        $result .= "t=top.document;
        if (!wikiovoted) {
          n = t.getElementById('wikionote1');
          a = t.getElementById('wikioaction2');
        if (n && !isNaN(parseInt(n.innerHTML))) n.innerHTML = parseInt(n.innerHTML)+1;
        if (a) a.innerHTML = '". $wikioPattern2 ."';
        }
        wikiovoted = true;";
      }
    }
    $result .= "}
    </script>";

    if ($button_type == 'big') {
      $result .= '
      <div class="wikiobutton">
        <a href="http://'. $wikioUrl . $wikioPattern3 . $wikioId . $wikiohasVoted . '" class="wikioaction" target="_tab" onclick="setwikio();">
          <div class="wikiotxt b">
            <div class="wikionote"><a id="wikionote" href="http://'. $wikioUrl . $wikioPattern3 . $wikioId . $wikiohasVoted .'" class="wikioaction" target="_tab" onclick="setwikio();">'. $wikioNote .'</a></div>
            <div class="wikioaction"><a href="http://'. $wikioUrl . $wikioPattern3 . $wikioId . $wikiohasVoted .'" class="wikioaction" id="wikioaction" target="_tab" onclick="setwikio();">'. $wikioPattern1 .'</a></div>
          </div>
        </a>
        <div class="wikio"><a href="http://'. $wikioUrl .'" target="_tab"><img src="http://'. $wikioUrl .'/shared/img/vote/wikio.gif" alt="www.wikio.'. $wikioSuffix .'" border="0" /></a></div>
      </div>
      ';
    } elseif ($button_type == 'inline') {
      $result .= '
      <div class="wikiobutton1">
				<div class="wikioaction1"><a href="http://'. $wikioUrl . $wikioPattern3 . $wikioId . $wikiohasVoted .'" target="_tab" id="wikioAction1" onclick="setWikio();">'. $wikioPattern1. '</a></div>
				<a href="http://'. $wikioUrl . $wikioPattern3 . $wikioId . $wikiohasVoted .'" class="wikiolink" target="_tab" onclick="setWikio();"><div class="wikiovote1" align="center" id="wikioNote1">'. $wikioNote .'</div></a>
			</div>
      ';
    }
  }

  return $result;
}
?>
